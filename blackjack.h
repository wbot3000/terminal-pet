#ifndef BLACKJACK_H_
#define BLACKJACK_H_

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "tpgraphics.h"

#define PLAY_TO 21
#define DEALER_DRAW_UP_TO 17 //Dealer hits if under this number, stands if greater than or equal to it

int drawCard(int* cardCounts);

void updateCardCount(WIN_DATA* cardWin, unsigned int userScore, unsigned int dealerScore);

int playBlackjack(int currMoney, WIN_DATA* titleWin, WIN_DATA* cardWin, WIN_DATA* infoWin, WIN_DATA* menuWin);

#endif /* BLACKJACK_H_ */
