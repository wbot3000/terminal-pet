//Terminal Pet Owner Header File
//Walker Bove
// 6-9-2021

#ifndef OWNER_H_
#define OWNER_H_

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_NUM_TREATS 999

typedef struct Owner {
	unsigned int money;
	unsigned int treats[3]; //Counts for each of the three treat types
} Owner;

Owner* initOwner();

void freeOwner(Owner* o);

int readOwnerFromFile(Owner* o, FILE* ownerinfo);

void purchaseTreat(Owner* o, unsigned int treatIndex);

#endif
