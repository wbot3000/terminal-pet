/*
 * tpgraphics.h
 *
 *  Created on: Aug 19, 2021
 *      Author: user
 */

#ifndef TPGRAPHICS_H_
#define TPGRAPHICS_H_

#include <menu.h>
#include <ncurses.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

/*
 * Macros
 */
#define BACKSPACE_KEY 8
#define ENTER_KEY 10
#define ONE_LINE_WINDOW_SIZE 3


/*
 * Structs
 */

/*
 * WIN_DATA holds a pointer to a window, as well as that window's height, width, starting x coordinate, and starting y coordinate
 */
typedef struct WIN_DATA {
	WINDOW* winptr;
	int xCoord, yCoord, width, height;
} WIN_DATA;

/*
 * SUBWIN_DATA holds two win_data pointers
 * 	-The first points to the data for a subwindow
 * 	-The second points to the data for the main window that the subwindow belongs to
 * The subwindow coordinates are relative to the main window, as derwin is used to create it
 */
typedef struct SUBWIN_DATA {
	WIN_DATA* data;
	WIN_DATA* main_win;
} SUBWIN_DATA;

/*
 * SCROLL_TEXT holds a WIN_DATA pointer, an array of strings, and the length of that string array. SCROLL_TEXT breaks up a string based on the size of the
 * WIN_DATA window provided
 */
typedef struct SCROLL_TEXT {
	WIN_DATA* displayWin;
	char** text;
	unsigned int numParts;
	unsigned int currSegment;
} SCROLL_TEXT;

/*
 * MENU_DATA holds the menu object, the items used to construct that menu object, the WIN_DATA* and SUBWIN_DATA* that the menu is displayed in
 */
typedef struct MENU_DATA {
	ITEM** itemList;
	unsigned int numChoices;
	MENU* menuptr;

	WIN_DATA* mainWin;
	SUBWIN_DATA* subWin;

	unsigned int currSelection;
} MENU_DATA;


/*
 * Functions
 */

/*
 * getStrHeight gets how many rows a string is designed to take up
 * getStrWidth gets the longest number of columns any row of characters takes up
 *
 * Both of these functions are designed to work with text-art
 */
unsigned int getStrHeight(char* str);

unsigned int getStrWidth(char* str);

/*
 * createWinData and deleteWinData create and delete win_data objects (pretty self explanatory)
 */
WIN_DATA* createWinData(int height, int width, int starty, int startx);

void deleteWinData(WIN_DATA* win_data);

/*
 * createSubWinData and deleteSubWinData create and delete subwin_data objects (also pretty self explanatory)
 * The window pointed to by the subwin_data is created using derwin, so coordinates are relative to the main window
 */
SUBWIN_DATA* createSubWinData(WIN_DATA* main_win, int height, int width, int starty, int startx);


void deleteSubWinData(SUBWIN_DATA* subwin_data);


/*
 * createScrollText creates a SCROLL_TEXT struct out of a string and WIN_DATA
 */

SCROLL_TEXT* createScrollText(WIN_DATA* win, char* str);

/*
 * deleteScrollText frees up all the memory used in creating a SCROLL_TEXT struct
 */
void deleteScrollText(SCROLL_TEXT* scrollText);

/*
 * postScrollText displays part (or all if it can fit) of the SCROLL_TEXT in its window
 */
void postScrollText(SCROLL_TEXT* scrollText);

/*
 * unpostScrollText clears out the window that the SCROLL_TEXT exists in
 */
void unpostScrollText(SCROLL_TEXT* scrollText);

/*
 * prevText displays the previous group of text if there is one
 */
void prevText(SCROLL_TEXT* scrollText, bool silent);

/*
 * nextText displays the next group of text if there is one
 */
void nextText(SCROLL_TEXT* scrollText, bool silent);

/*
 * scrollTextToBeginning sets the SCROLL_TEXT to the first section
 */
void scrollTextToBeginning(SCROLL_TEXT* scrollText, bool silent);

/*
 * scrollTextToEnd sets the SCROLL_TEXT to the end section
 */
void scrollTextToEnd(SCROLL_TEXT* scrollText, bool silent);

/*
 * createMenuData creates a MENU_DATA object given a WIN_DATA*, string array of menu options, and the length of that array
 */
MENU_DATA* createMenuData(WIN_DATA* menuWindow, char** menuOptions, unsigned int numChoices);

/*
 * createMenuDataWithOptions creates a MENU_DATA object given a WIN_DATA*, string array of menu options, and the length of that array
 */
MENU_DATA* createMenuDataWithOptions(WIN_DATA* menuWindow, char** menuOptions, char** menuDescs, unsigned int numChoices);

/*
 *	deleteMenuData frees up any memory allocated for a specified MENU_DATA object (Note: does NOT free main window that the menu took up, only the subwindow that it created)
 */

void deleteMenuData(MENU_DATA* menuData);


/*
 *	Creates a simple text prompt on screen that can be scrolled through and dismissed
 */
void basicTextPrompt(SCROLL_TEXT* scrollText);

/*
 * basicTextPromptStr creates a simple text prompt when supplied with a WIN_DATA and a string
 */
void basicTextPromptStr(WIN_DATA* WinData, char* text);

/*
 * textPromptWithMenu creates a text prompt and a menu. Returns the menu option selected
 */
void textPromptWithMenu(SCROLL_TEXT* scrollText, MENU_DATA* menuData);


/*
 * textPromptWithMenuStr takes a WIN_DATA* and a string and turns it into SCROLL_TEXT, instead of taking the SCROLL_TEXT pointer directly. This new SCROLL_TEXT struct is deleted afterwards.
 */
void textPromptWithMenuStr(WIN_DATA* infoWin, char* str, MENU_DATA* menuData);


/*
 * textPromptTypeResponse displays a SCROLL_TEXT object along with a typing prompt to fill in a string
 */
void textPromptTyped(SCROLL_TEXT* scrollText, WIN_DATA* typingWin, char* writeTo, unsigned int maxChars);

/*
 *  textPromptTypeResponseTemp displays a SCROLL_TEXT object along with a typing prompt to fill in a string. SCROLL_TEXT is deleted after
 */
void textPromptTypedTemp(WIN_DATA* infoWin, char* text, WIN_DATA* typingWin, char* writeTo, unsigned int maxChars);

/*
 * Centers text within a window
 */
void centerInWindow(WIN_DATA* win_data, char* text);



/*
 * printStutteredText prints as much of a message that can fit within a window, waits for waitSec seconds, and then prints out more text
 * Prints out text until their is no more to print
 */
/*
void printStutteredText(WIN_DATA* win_data, char* text, unsigned int waitSec) {
	const int rowBorder = win_data->height - 1;
	const int colBorder = win_data->width - 1;

	if(rowBorder < 1 || colBorder < 1) { //Can't print anything if window has no interior
		return;
	}

	int currY = 1;
	int currX = 1;

	while(*text != '\0') {
		if(currY >= rowBorder) { //Can't fit anymore onto screen, wait waitSec seconds, then print more over current
			wrefresh(win_data->winptr);
			sleep(waitSec);
			currY = 1;
			//currX will already be 1 due to next condition
		}
		else if(*text == '\n' || currX >= colBorder) {
			currX = 1;
			currY++;
		}

		if(currY < rowBorder) { //Prevents printing if the cursor ends up out of bounds
			mvwaddch(win_data->winptr, currY, currX, *text);
			currX++;
			text++;
		}

	}

	while(currY < rowBorder) { //Fill up the rest of the box with empty space
		if(currX >= colBorder) {
			currX = 1;
			currY++;
		}

		if(currY < rowBorder) {
			mvwaddch(win_data->winptr, currY, currX, ' ');
			currX++;
			text++;
		}
	}

	wrefresh(win_data->winptr);
	sleep(waitSec);
	for(int y = 1; y < rowBorder; ++y) { //Clear box
		for(int x = 1; x < colBorder; ++x) {
			mvwaddch(win_data->winptr, y, x, ' ');
		}
	}
	wrefresh(win_data->winptr);
}*/

/*
 * eraseText erases text from a window
 */
void eraseText(WIN_DATA* win_data);


#endif /* TPGRAPHICS_H_ */
