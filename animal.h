//Terminal Pet Animal Header File
//Walker Bove
// 6-9-2021

#ifndef ANIMAL_H_
#define ANIMAL_H_

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define NAME_MAX 128
#define SPECIES_MAX 128
#define MAX_COLOR_NUM_DIGITS 4
#define BODY_MAX 65536

#define ANIMAL_COLOR_PAIR 1

extern char* animalTypes[];
extern char* animalBodies[];


typedef struct Animal {
	//Traits
	char* name;
	char* species;
	int color;
	char* body;

	//Stats
	int hunger;
	int happiness;
} Animal;

Animal* initAnimal();

void freeAnimal(Animal* a);

int readAnimalFromFile(Animal* a, FILE* petinfo);

/*
void printAnimal(Animal* a) {
	printf("\e[%dm%s\e[39m\n", a->color, a->body);
}*/

void updateAnimalHunger(Animal* a, int hungerInc);

void updateAnimalHappiness(Animal* a, int happinessInc);

void printAnimalInfo(Animal* a);

#endif
