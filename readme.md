**Welcome to Terminal Pet!**

How To Use:
-------------------------------------------------------
You interact with your teminal pet by navigating menus.

**Up Arrow- Move Selection Up**

**Down Arrow- Move Selection Down**

**Enter- Select Menu Choice**


When text appears in the "blurb" section (the one below the pet and above the menu),
if a piece of text is too large to fit in the box, only part of it will display at a given moment. Use the page keys to "scroll" the text

**Enter- Dismiss text prompt**

**Page Up- Display a previous part of a larger text**

**Page Down- Display the next part of a larger text**


Terminal Pet Basics:
-------------------------------------------------------
When you start up Terminal Pet for the first time, you are taken through a setup which lets you create a pet. You can choose from five species (dog, cat, bird, fish, or alien), and seven colors (red, green, yellow, blue, magenta, cyan, or white). You can then give your pet a name of your choice.

**NOTE: Currently, Terminal Pet stores data in files created in its directory. If you move the location of the executable, then Terminal Pet will think you don't have a pet and create a new set of files.**

Your Terminal Pet has both a Hunger stat and a Happiness stat, both of which decrease over time. You can increase your pet's hunger by feeding it, and their happiness by playing games with them or feeding then purchased treats.


Games:
-------------------------------------------------------
Terminal Pet comes with two games.

**In the Addition Game, you have to answer as many simple addition questions as possible before a 60 second time limit.**

**In Blackjack, you play against a computer dealer to see which one of you can get closest to 21 without going over.**

By playing these games, you can win money, which you can spend on treats in the shop.


Shop:
-------------------------------------------------------
In the shop, you can buy your pet treats. While you have an unlimited supply of grub to feed your pet in case they get hungry, this gruel isn't very tasty, and will decrease your pet's total happiness. There are three types of treats.

**Basic Treats, which increase hunger and happiness by a moderate amount.**

**Filling Treats, which don't increase happiness, but increase hunger by a lot.**

**Luxury Treats, which increase happiness by a lot, and hunger slightly more than Basic Treats, but are the most expensive.**


Etc.
-------------------------------------------------------
Thank you for checking out Terminal Pet. I'm continually working on this project, hoping to add even more features as time goes on.
