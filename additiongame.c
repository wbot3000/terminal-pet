/*
 * additiongame.c
 *
 *  Created on: Sep 20, 2021
 *      Author: user
 */

#include "additiongame.h"

volatile sig_atomic_t additionSignalCaught = false;

void alarmSet(int alarmSignal) {
	additionSignalCaught = true;
}

void generateNewNumbers(int* first, int* second, int* total) { //Generate a new set of numbers
	*first = rand() % 10 + 1;
	*second = rand() % 10 + 1;
	*total = *first + *second;
}

void updateScoreboard(WIN_DATA* scoreWin, char* scoreBuffer, int score, bool correct) {
	eraseText(scoreWin);
	if(score < 0) { //Used to set up scoreboard at the beginning of the game
		sprintf(scoreBuffer, "Score: 0\nGood luck!");
	}
	else if(!correct) {
		sprintf(scoreBuffer, "Score: %d\nIncorrect...", score);
	}
	else {
		sprintf(scoreBuffer, "Score: %d\nCorrect!", score);
	}
	centerInWindow(scoreWin, scoreBuffer);
}

/*
 * The function that actually plays the Addition Game. Make sure to use this in a program that has called initscr(), else it won't work properly
 */
unsigned int playAdditionGame(WIN_DATA* titleWin, WIN_DATA* scoreWin, WIN_DATA* eqDisplayWin, WIN_DATA* inputWin) {
	unsigned int score = 0;
	int firstNum;
	int secondNum;
	int total; //Correct answer for question
	int userInputNum; //User input converted into an int

	char input[USER_BUFSIZE]; //Used to take input
	char eqBuf[EQ_BUFSIZE]; //Used to store the printed equation
	char scoreBuf[SCORE_BUFSIZE]; //Used to store the printed score

	srand(time(NULL)); //Seed the random number generator
	generateNewNumbers(&firstNum, &secondNum, &total); //Generate first set of random numbers and but them in addresses

	//Initialize sigaction and zero out the memory
	struct sigaction alarmAction;
	memset(&alarmAction, 0, sizeof(struct sigaction));

	alarmAction.sa_handler = alarmSet;
    if (sigaction(SIGALRM, &alarmAction, NULL) == -1) {
    	basicTextPromptStr(eqDisplayWin, "Error: Game cannot be started. Timer cannot be set.");
        return 0;
    }

    /*
     * Erase text in all windows so that text can be written to them
     */
    eraseText(titleWin);
    eraseText(scoreWin);
    eraseText(eqDisplayWin);
    eraseText(inputWin);

    centerInWindow(titleWin, "Addition Game");

    basicTextPromptStr(eqDisplayWin, "Answer as many addition questions as possible within 60 seconds. Good luck! (Press enter to start the timer)");

    updateScoreboard(scoreWin, scoreBuf, -1, true);
	alarm(GAME_TIME_SECS);
	while(!additionSignalCaught) {
		sprintf(eqBuf, "%d + %d = ", firstNum, secondNum); //Print out question
		eraseText(inputWin); //Erase input from previous question
		textPromptTypedTemp(eqDisplayWin, eqBuf, inputWin, input, USER_BUFSIZE);
		userInputNum = atoi(input); //Convert user input string into int
		if(!additionSignalCaught) { //Don't print correct or incorrect if time runs out
			if(total == userInputNum) { //User writes correct number
				score++;
				updateScoreboard(scoreWin, scoreBuf, score, true);
			}
			else {
				updateScoreboard(scoreWin, scoreBuf, score, false);
			}
		}
		generateNewNumbers(&firstNum, &secondNum, &total); //Generate a new set of numbers for the next problem.
	}
	char gameOverString[SCORE_BUFSIZE];
	sprintf(gameOverString, "Time's up! Your score is: %d", score);
	basicTextPromptStr(eqDisplayWin, gameOverString);

    eraseText(titleWin);
    eraseText(scoreWin);
    eraseText(eqDisplayWin);
    eraseText(inputWin);

    additionSignalCaught = false;

	return score;

}
