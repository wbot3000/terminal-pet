/*
 * animal.c
 *
 *  Created on: Sep 20, 2021
 *      Author: user
 */
#include "animal.h"

char* animalTypes[] = {"dog", "cat", "bird", "fish", "alien"};
char* animalBodies[] = {
		"  __________\n"
		" /|        |\\\n"
		"/ |   0   0| \\\n"
		"| |     O  | |\n"
		"  |     W  |\n"
		"  \\________/\n"
		"   |      |\n"
		" \\ |      |\n"
		"  \\|      |\n"
		"   \\______/\n"
		"   | |  | |\n"
		"  /__\\  /__\\\n",

"  /\\_____/\\\n"
"  |       |\n"
"  |  0 . 0|\n"
"  |    W  |\n"
"  \\_______/\n"
"   |     |\n"
"  \\|     |\n"
"   \\_____/\n"
"   ||   ||\n"
"  /_\\   /_\\\n",

"  ___\n"
" / 0 \\\n"
" |   |>\n"
" /   \\\n"
"<|   |>\n"
" \\___/\n"
" |   |\n"
" M   M\n",

"     ____\n"
"    |____\\__\n"
"\\  /        \\\n"
" \\    <    O \\\n"
" /          __|\n"
"/  \\_________/\n",

"  O      O\n"
"   \\    /\n"
"   __--__\n"
"  |      |\n"
"  | o O o|\n"
"  |   u  |\n"
"| \\______/ |\n"
"\\/ |    | \\/\n"
"  _/    \\_\n"};

Animal* initAnimal() {
	Animal* pet = (Animal*)malloc(sizeof(Animal));
	if(pet == NULL) {
		goto RELEASE_FROM_PET;
	}

	pet->name = (char*)malloc(sizeof(char)*NAME_MAX+1);
	if(pet->name == NULL) {
		goto RELEASE_FROM_NAME;
	}

	pet->species = (char*)malloc(sizeof(char)*SPECIES_MAX+1);
	if(pet->species == NULL) {
		goto RELEASE_FROM_SPECIES;
	}

	pet->color = 7;

	pet->body = (char*)malloc(sizeof(char)*BODY_MAX+1);
	if(pet->body == NULL) {
		goto RELEASE_FROM_BODY;
	}

	pet->hunger = 100;
	pet->happiness = 100;
	return pet; //Allocating all memory works, return pet

	//Case where something fails to malloc, return NULL
	RELEASE_FROM_BODY: free(pet->species);
	RELEASE_FROM_SPECIES: free(pet->name);
	RELEASE_FROM_NAME: free(pet);
	RELEASE_FROM_PET: return NULL;

}

void freeAnimal(Animal* a) {
	free(a->name);
	free(a->species);
	free(a->body);
	free(a);
}

int readAnimalFromFile(Animal* a, FILE* petinfo) { //Returns EXIT_SUCCESS is reading is successful, else EXIT_FAILURE
	fgets(a->name, NAME_MAX + 1, petinfo); //Read name
	if(ferror(petinfo) != 0) {
		fprintf(stderr, "Error: Name could not be read from .petinfo.txt. %s\n", strerror(errno));
		freeAnimal(a);
		return EXIT_FAILURE;
	}
	else {
		a->name[strcspn(a->name, "\n")] = '\0'; //Remove \n
	}

	fgets(a->species, SPECIES_MAX + 1, petinfo); //Read species
	if(ferror(petinfo) != 0) {
		fprintf(stderr, "Error: Species could not be read from .petinfo.txt. %s\n", strerror(errno));
		freeAnimal(a);
		return EXIT_FAILURE;
	}
	else {
		a->species[strcspn(a->species, "\n")] = '\0'; //Remove \n
	}

	char colorNumberBuffer[MAX_COLOR_NUM_DIGITS];
	fgets(colorNumberBuffer, MAX_COLOR_NUM_DIGITS, petinfo);
	if(ferror(petinfo) != 0) {
		fprintf(stderr, "Error: Color number could not be read from .petinfo.txt. %s\n", strerror(errno));
		freeAnimal(a);
		return EXIT_FAILURE;
	}
	else {
		a->color = atoi(colorNumberBuffer);
	}

	fread(a->body, sizeof(char), BODY_MAX + 1, petinfo);
	if(ferror(petinfo) != 0) { //Error indicator for file set
		fprintf(stderr, "Error: Body could not be read from .petinfo.txt.\n");
		freeAnimal(a);
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

/*
void printAnimal(Animal* a) {
	printf("\e[%dm%s\e[39m\n", a->color, a->body);
}*/

void updateAnimalHunger(Animal* a, int hungerInc) {
	a->hunger += hungerInc;
	if(a->hunger < 0) {
		a->hunger = 0;
	}
	if(a->hunger > 100) {
		a->hunger = 100;
	}
}

void updateAnimalHappiness(Animal* a, int happinessInc) {
	a->happiness += happinessInc;
	if(a->happiness < 0) {
		a->happiness = 0;
	}
	if(a->happiness > 100) {
		a->happiness = 100;
	}
}

void printAnimalInfo(Animal* a) {
	printf("Hunger: %d\nHappiness: %d\n\n", a->hunger, a->happiness);
}

