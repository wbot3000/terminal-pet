/*
 * owner.c
 *
 *  Created on: Sep 20, 2021
 *      Author: user
 */

#include "owner.h"

Owner* initOwner() {
	Owner* o = (Owner*)malloc(sizeof(Owner));
	if(o == NULL) {
		return NULL;
	}
	o->money = 0;
	for(int i = 0; i < 3; i++) {
		o->treats[i] = 0;
	}
	return o;
}

void freeOwner(Owner* o) {
	free(o);
}

int readOwnerFromFile(Owner* o, FILE* ownerinfo) {
	char moneyBuf[11];
	fgets(moneyBuf, 11, ownerinfo); //Read in money value
	if(ferror(ownerinfo) != 0) {
		fprintf(stderr, "Error: Owner money could not be read from .ownerinfo.txt. %s\n", strerror(errno));
		freeOwner(o);
		return EXIT_FAILURE;
	}
	else {
		o->money = atoi(moneyBuf);
	}
	for(int i = 0; i < 3; i++) { //Read in all the treat counts
		char treatBuf[4];
		fgets(treatBuf, 4, ownerinfo);
		if(ferror(ownerinfo) != 0) {
			fprintf(stderr, "Error: Treat counts could not be read from .ownerinfo.txt. %s\n", strerror(errno));
			freeOwner(o);
			return EXIT_FAILURE;
		}
		else {
			o->treats[i] = atoi(treatBuf);
		}
	}
	return EXIT_SUCCESS;
}

void purchaseTreat(Owner* o, unsigned int treatIndex) {
	if(o->treats[treatIndex] < MAX_NUM_TREATS) {
		o->treats[treatIndex]++;
	}
}
