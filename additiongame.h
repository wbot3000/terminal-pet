#ifndef ADDITIONGAME_H_
#define ADDITIONGAME_H_

#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "tpgraphics.h"

#define GAME_TIME_SECS 60 //Game will take one minute (60 seconds)
#define TEXT_STUTTER_LEN 3 //How long text will remain on screen if it won't fit into single window
#define USER_BUFSIZE 256 //Buffer size for the user input
#define EQ_BUFSIZE 16 //Buffer size for the written equation
#define SCORE_BUFSIZE 64 //Buffer size for the written score

extern volatile sig_atomic_t additionSignalCaught;

void alarmSet(int alarmSignal);

void generateNewNumbers(int* first, int* second, int* total);

void updateScoreboard(WIN_DATA* scoreWin, char* scoreBuffer, int score, bool correct);

/*
 * The function that actually plays the Addition Game. Make sure to use this in a program that has called initscr(), else it won't work properly
 */
unsigned int playAdditionGame(WIN_DATA* titleWin, WIN_DATA* scoreWin, WIN_DATA* eqDisplayWin, WIN_DATA* inputWin);

#endif /*ADDITIONGAME_H_*/
