/*
 * tpgraphics.c
 *
 *  Created on: Sep 20, 2021
 *      Author: user
 */

#include "tpgraphics.h"

unsigned int getStrHeight(char* str) {
	unsigned int strHeight = 0;
	char* prevChar;
	while(*str != '\0') {
		if(*str == '\n') {
			strHeight++;
		}
		prevChar = str;
		str++;
	}
	if(*prevChar != '\n') {
		strHeight++;
	}
	return strHeight;
}

unsigned int getStrWidth(char* str) {
	unsigned int strWidth = 0;
	unsigned int currLineWidth = 0;
	while(*str != '\0') {
		if(*str == '\n') {
			if(currLineWidth > strWidth) {
				strWidth = currLineWidth;
			}
			currLineWidth = 0;
		}
		else {
			currLineWidth++;
		}
		str++;
	}
	return strWidth;
}


WIN_DATA* createWinData(int height, int width, int starty, int startx) {
	/*
	 * Set fields of win_data
	 */
	WIN_DATA *win_data = (WIN_DATA*)malloc(sizeof(WIN_DATA));
	win_data->winptr = newwin(height, width, starty, startx);
	win_data->xCoord = startx;
	win_data->yCoord = starty;
	win_data->width = width;
	win_data->height = height;

	/*
	 * Display win_data's window
	 */
	box(win_data->winptr, 0, 0);
	wrefresh(win_data->winptr);
	return win_data;
}

void deleteWinData(WIN_DATA* win_data) {
	wborder(win_data->winptr, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	wrefresh(win_data->winptr);
	delwin(win_data->winptr);
	free(win_data);
}


SUBWIN_DATA* createSubWinData(WIN_DATA* main_win, int height, int width, int starty, int startx) {
	SUBWIN_DATA *subwin_data = (SUBWIN_DATA*)malloc(sizeof(WIN_DATA));
	subwin_data->data = (WIN_DATA*)malloc(sizeof(WIN_DATA));
	subwin_data->data->winptr = derwin(main_win->winptr, height, width, starty, startx);
	subwin_data->data->xCoord = startx;
	subwin_data->data->yCoord = starty;
	subwin_data->data->width = width;
	subwin_data->data->height = height;
	subwin_data->main_win = main_win;

	return subwin_data;
}


void deleteSubWinData(SUBWIN_DATA* subwin_data) {
	deleteWinData(subwin_data->data);
	free(subwin_data);
}


SCROLL_TEXT* createScrollText(WIN_DATA* win, char* str) {
	SCROLL_TEXT* scrollText = (SCROLL_TEXT*)malloc(sizeof(SCROLL_TEXT));
	scrollText->displayWin = win;
	const unsigned int winArea = (win->width - 2) * (win->height - 2);
	const unsigned int strLength = strlen(str);
	scrollText->numParts = strLength/winArea + 1;
	scrollText->currSegment = 0;
	scrollText->text = (char**)malloc(scrollText->numParts * sizeof(char*));

	for(int i = 0; i < scrollText->numParts; i++) {
		scrollText->text[i] = (char*)malloc((winArea+1) * sizeof(char));
		strncpy(scrollText->text[i], str, winArea);
		scrollText->text[i][winArea + 1] = '\0';
		str += winArea;
	}
	return scrollText;
}


void deleteScrollText(SCROLL_TEXT* scrollText) {
	for(int i = 0; i < scrollText->numParts; i++) {
		free(scrollText->text[i]);
	}
	free(scrollText->text);
	free(scrollText);
}


void postScrollText(SCROLL_TEXT* scrollText) {
	char* displayText = scrollText->text[scrollText->currSegment];
	int y = 1;
	int x = 1;
	while(y < scrollText->displayWin->height && *displayText != '\0') {
		if(x >= (scrollText->displayWin->width) - 1) {
			x = 1;
			y++;
		}
		mvwaddch(scrollText->displayWin->winptr, y, x, *displayText);
		x++;
		displayText++;
	}
	for(; y < scrollText->displayWin->height; y++) { //Fill the rest of the area with spaces
		for(; x < (scrollText->displayWin->width) - 1; x++) {
			mvwaddch(scrollText->displayWin->winptr, y, x, ' ');
		}
	}
	wrefresh(scrollText->displayWin->winptr);
}


void unpostScrollText(SCROLL_TEXT* scrollText) {
	for(int y = 1; y < (scrollText->displayWin->height) - 1; y++) {
		for(int x = 1; x < (scrollText->displayWin->width) - 1; x++) {
			mvwaddch(scrollText->displayWin->winptr, y, x, ' ');
		}
	}
}


void prevText(SCROLL_TEXT* scrollText, bool silent) {
	if(scrollText->currSegment <= 0) {
		return;
	}
	scrollText->currSegment--;
	if(!silent) {
		postScrollText(scrollText);
	}
}


void nextText(SCROLL_TEXT* scrollText, bool silent) {
	if(scrollText->currSegment >= scrollText->numParts - 1) {
		return;
	}
	scrollText->currSegment++;
	if(!silent) {
		postScrollText(scrollText);
	}
}


void scrollTextToBeginning(SCROLL_TEXT* scrollText, bool silent) {
	scrollText->currSegment = 0;
	if(!silent) {
		postScrollText(scrollText);
	}
}


void scrollTextToEnd(SCROLL_TEXT* scrollText, bool silent) {
	scrollText->currSegment = scrollText->numParts - 1;
	if(!silent) {
		postScrollText(scrollText);
	}
}


MENU_DATA* createMenuData(WIN_DATA* menuWindow, char** menuOptions, unsigned int numChoices) {
	MENU_DATA* menuData = (MENU_DATA*)malloc(sizeof(MENU_DATA));

	menuData->itemList = (ITEM**)calloc(numChoices, sizeof(ITEM*));
	for(int i = 0; i < numChoices; ++i) {
		menuData->itemList[i] = new_item(menuOptions[i], "");
	}
	menuData->numChoices = numChoices;

	menuData->menuptr = new_menu((ITEM**)menuData->itemList);
	set_menu_mark(menuData->menuptr, ">");

	menuData->mainWin = menuWindow;
	set_menu_win(menuData->menuptr, menuData->mainWin->winptr);

	menuData->subWin = createSubWinData(menuData->mainWin, menuData->mainWin->height - 2, menuData->mainWin->width - 2, 1, 1);
	set_menu_sub(menuData->menuptr, menuData->subWin->data->winptr);

	menuData->currSelection = 1;

	return menuData;
}


MENU_DATA* createMenuDataWithOptions(WIN_DATA* menuWindow, char** menuOptions, char** menuDescs, unsigned int numChoices) {
	MENU_DATA* menuData = (MENU_DATA*)malloc(sizeof(MENU_DATA));

	menuData->itemList = (ITEM**)calloc(numChoices, sizeof(ITEM*));
	for(int i = 0; i < numChoices; ++i) {
		menuData->itemList[i] = new_item(menuOptions[i], menuDescs[i]);
	}
	menuData->numChoices = numChoices;

	menuData->menuptr = new_menu((ITEM**)menuData->itemList);
	set_menu_mark(menuData->menuptr, ">");

	menuData->mainWin = menuWindow;
	set_menu_win(menuData->menuptr, menuData->mainWin->winptr);

	menuData->subWin = createSubWinData(menuData->mainWin, menuData->mainWin->height - 2, menuData->mainWin->width - 2, 1, 1);
	set_menu_sub(menuData->menuptr, menuData->subWin->data->winptr);

	menuData->currSelection = 1;

	return menuData;
}


void deleteMenuData(MENU_DATA* menuData) {
	unpost_menu(menuData->menuptr);
	free_menu(menuData->menuptr);
	for(int i = 0; i < menuData->numChoices; ++i) {
		free_item(menuData->itemList[i]);
	}
	free(menuData->itemList);

	deleteSubWinData(menuData->subWin);
}


void basicTextPrompt(SCROLL_TEXT* scrollText) {
	int input;
	postScrollText(scrollText);

	while((input = wgetch(scrollText->displayWin->winptr)) != ENTER_KEY) {
		switch(input) {
			case KEY_PPAGE:
				prevText(scrollText, false);
				break;
			case KEY_NPAGE:
				nextText(scrollText, false);
				break;
			case ENTER_KEY:
				unpostScrollText(scrollText);
				break;
			default:
				break;
		}
	}
}


void basicTextPromptStr(WIN_DATA* WinData, char* text) {
	SCROLL_TEXT* scrollText = createScrollText(WinData, text);
	basicTextPrompt(scrollText);
	deleteScrollText(scrollText);
}


void textPromptWithMenu(SCROLL_TEXT* scrollText, MENU_DATA* menuData) {
	post_menu(menuData->menuptr);
	postScrollText(scrollText);
	wrefresh(menuData->mainWin->winptr);

	int userInput;
	//int selection = menuData->currSelection;

	while((userInput = wgetch(menuData->mainWin->winptr)) != ENTER_KEY){
		switch(userInput)
		{	case KEY_DOWN:
				menu_driver(menuData->menuptr, REQ_DOWN_ITEM);
				if(menuData->currSelection < menuData->numChoices - 1) {
					menuData->currSelection++;
				}
				break;
			case KEY_UP:
				menu_driver(menuData->menuptr, REQ_UP_ITEM);
				if(menuData->currSelection > 1) {
					menuData->currSelection--;
				}
				break;
			case KEY_PPAGE:
				prevText(scrollText, false);
				break;
			case KEY_NPAGE:
				nextText(scrollText, false);
				break;
			default:
				break;
		}
		wrefresh(menuData->mainWin->winptr);
	}
	unpost_menu(menuData->menuptr);
	unpostScrollText(scrollText);
	//return selection;
}


void textPromptWithMenuStr(WIN_DATA* infoWin, char* str, MENU_DATA* menuData) {
	SCROLL_TEXT* scrollText = createScrollText(infoWin, str);
	textPromptWithMenu(scrollText, menuData);
	deleteScrollText(scrollText);
}


void textPromptTyped(SCROLL_TEXT* scrollText, WIN_DATA* typingWin, char* writeTo, unsigned int maxChars) {
	int input;
	unsigned int y = 1;
	unsigned int x = 1;

	int charCounter = 0; //Keeps track of current /0 position
	writeTo[0] = '\0';

	curs_set(2);
	wmove(typingWin->winptr, 1, 1);
	postScrollText(scrollText);
	while((input = wgetch(typingWin->winptr)) != ENTER_KEY && input != ERR) {
		switch(input) {
			case BACKSPACE_KEY:
				if(charCounter > 0) {
					writeTo[charCounter] = '\0';
					charCounter--;
					if(x <= 1) {
						y--;
						x = typingWin->width - 1;
					}
					else {
						x--;
					}
					wmove(typingWin->winptr, y, x);
					wdelch(typingWin->winptr);
				}
				break;
			case ENTER_KEY:
				break;
			case ERR:
				break;
			case KEY_PPAGE:
				prevText(scrollText, false);
				break;
			case KEY_NPAGE:
				nextText(scrollText, false);
				break;
			default:
				if(charCounter < maxChars && ((input >= 33 && input <= 126) || (input = (int)' ')) ) {
					writeTo[charCounter] = (char)input;
					charCounter++;
					writeTo[charCounter] = '\0';
					if(y <= typingWin->height - 2) {
						mvwaddch(typingWin->winptr, y, x, (char)input);
					}

					if(x >= typingWin->width - 2) {
						if(y <= typingWin->height - 2) {
							x = 1;
							y++;
						}
					}
					else {
						x++;
					}

					wmove(typingWin->winptr, y, x);
				}
				break;
		}
		wrefresh(typingWin->winptr);
	}
	curs_set(0);
	unpostScrollText(scrollText);
}


void textPromptTypedTemp(WIN_DATA* infoWin, char* text, WIN_DATA* typingWin, char* writeTo, unsigned int maxChars) {
	SCROLL_TEXT* prompt = createScrollText(infoWin, text);
	textPromptTyped(prompt, typingWin, writeTo, maxChars);
	deleteScrollText(prompt);
}


void centerInWindow(WIN_DATA* win_data, char* text) {
	const int startX = (win_data->width - getStrWidth(text))/2;
	int currY = (win_data->height - getStrHeight(text))/2;
	int currX = startX;
	while(*text != '\0') {
		if(*text == '\n') {
			currX = startX;
			currY++;
		}
		else {
			mvwaddch(win_data->winptr, currY, currX, *text);
			currX++;
		}
		text++;
	}
	wrefresh(win_data->winptr);
}


void eraseText(WIN_DATA* win_data) {
	for(int y = 1; y < (win_data->height - 1); ++y) {
		for(int x = 1; x < (win_data->width - 1); ++x) {
			mvwaddch(win_data->winptr, y, x, ' ');
		}
	}
}
