/*
 * tputility.h
 *
 *  Created on: Sep 5, 2021
 *      Author: user
 */

#ifndef TPUTILITY_H_
#define TPUTILITY_H_

#include <errno.h>
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "animal.h"
#include "owner.h"
#include "tpgraphics.h"

#define SECONDS_PER_ONE_HUNGER_DECREASE 600
/*
 * This file contains any extra functions used within the main file
 */

/*
 * Functions that involve reading and writing files
 */
int checkTimePassed(Animal* a, time_t* prevTime, char* timeFilepath);

void writePetStatus(Animal* a, char* statusFilepath);

void writeOwnerStats(Owner* o, char* ownerFilepath);


/*
 * Functions that involve printing text / Setting up text options
 */
void printPet(WIN_DATA* printWin, Animal* pet, int colorPair);

/*
 * Functions used for feeding the pet
 */
void displayPetHungerInfo(WIN_DATA* displayWin, Animal* a, Owner* o);

void feedPetMenu(WIN_DATA* infoWin, WIN_DATA* menuWin, Animal* a, Owner* o);

void shopMenu(WIN_DATA* infoWin, WIN_DATA* menuWin, Owner* o);


#endif /* TPUTILITY_H_ */
