/*
 * blackjack.c
 *
 *  Created on: Sep 20, 2021
 *      Author: user
 */

#include "blackjack.h"

int drawCard(int* cardCounts) { //"Draw" a new card
	int cardNum;
	bool numberValid = false;
	while(!numberValid) {
		cardNum = rand() % 13; //All card values (ace to king) equally likely
		if(cardNum > 9) { //10, Jack, Queen, and King all held within same value of cardCounts, since they all have a point value of 10
			cardNum = 9;
		}
		int check = cardCounts[cardNum];
		if(check > 0) { //If all the cards of that value haven't been drawn
			numberValid = true;
			cardCounts[cardNum]--;
		}
	}
	if(cardNum == 0) { //Set cardNum to 11 if ace for convenience
		cardNum = 11;
	}
	else {
		cardNum++; //True card number is one more than the index of the array
	}
	return cardNum;
}

void updateCardCount(WIN_DATA* cardWin, unsigned int userScore, unsigned int dealerScore) {
	char scoreBuf[19];
	sprintf(scoreBuf, "You: %d\n\nDealer: %d", userScore, dealerScore);
	eraseText(cardWin);
	centerInWindow(cardWin, scoreBuf);
}

int playBlackjack(int currMoney, WIN_DATA* titleWin, WIN_DATA* cardWin, WIN_DATA* infoWin, WIN_DATA* menuWin) {

	if(currMoney <= 0) {
		basicTextPromptStr(infoWin, "You can't bet anything. You have no money!");
		return 0;
	}

	int betAmount = 0;
	int winnings;
	char betString[10]; //Max int digits
	bool acceptableBet = false;

	while(!acceptableBet) {
		textPromptTypedTemp(infoWin, "What would you like to bet? You need to bet at least $1. Type back to go back to the main menu.", menuWin, betString, 10);
		if(strcmp(betString, "back") == 0) {
			return 0;
		}
		else {
			betAmount = atoi(betString);
			if(betAmount > currMoney) {
				basicTextPromptStr(infoWin, "You don't have enough money to bet that!");
			}
			else if(betAmount <= 0) {
				basicTextPromptStr(infoWin, "You can't bet that!");
			}
			else {
				basicTextPromptStr(infoWin, "Got it! Now let's play Blackjack.");
				acceptableBet = true;
			}
		}
	}

	srand(time(NULL));

	int cardCounts[] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 16}; //Keeps track of how many of each card is left. Four of Ace to 10, Twelve Face Cards (10 and Face at same index)

	int userCurrentCard;
	int userCount = 0; //User's total card value
	int userElevenAces = 0; //Aces that are counted as elevens for user
	bool keepGoing = true;
	bool naturalWin = true; //Determines if blackjack gotten on first turn. If so, increase payout by 1.5x

	int dealerCurrentCard;
	int dealerCount = 0;
	int dealerElevenAces = 0; //Aces that are counted as elevens for dealer

    eraseText(titleWin);
    eraseText(cardWin);
    eraseText(infoWin);
    eraseText(menuWin);

    centerInWindow(titleWin, "Blackjack");
    updateCardCount(cardWin, 0, 0);
    basicTextPromptStr(infoWin, "You and the dealer draw cards. Whoever gets closest without going over 21 wins. Good luck!");

	//Draw the first card for the user
	userCurrentCard = drawCard(cardCounts);
	if(userCurrentCard == 11) {
		basicTextPromptStr(infoWin, "Your first card is an ace.");
		userElevenAces++;
	}
	else {
		char userFirstCard[25]; //25 = max number of chars needed for string (for when card is a 10)
		sprintf(userFirstCard, "Your first card is a %d.", userCurrentCard);
		basicTextPromptStr(infoWin, userFirstCard);
	}
	userCount += userCurrentCard;
	updateCardCount(cardWin, userCount, dealerCount);

	//Draw the second card for the user
	userCurrentCard = drawCard(cardCounts);
	if(userCurrentCard == 11) {
		basicTextPromptStr(infoWin, "Your second card is an ace.");
		userElevenAces++;
	}
	else {
		char userSecondCard[26]; //26 = max number of chars needed for string (for when card is a 10)
		sprintf(userSecondCard, "Your second card is a %d.", userCurrentCard);
		basicTextPromptStr(infoWin, userSecondCard);
	}
	userCount += userCurrentCard;
	updateCardCount(cardWin, userCount, dealerCount);

	//Check if user got a natural blackjack
	if(userCount == PLAY_TO) {
		basicTextPromptStr(infoWin, "Wow! A natural blackjack!");
		keepGoing = false;
	}
	else {
		naturalWin = false;
	}

	//Check if user got two aces in a row (userCount will be a 22), and reduce it to 11.
	if(userCount > PLAY_TO) {
		userCount -= 10;
		userElevenAces--; //Only one ace counts for eleven, not both of them
	}

	//Draw the first card for the dealer
	dealerCurrentCard = drawCard(cardCounts);
	if(dealerCurrentCard == 11) {
		basicTextPromptStr(infoWin, "The dealer's first card is an ace.");
		dealerElevenAces++;
	}
	else {
		char dealerFirstCard[33]; //33 = max number of chars needed for string (for when card is a 10)
		sprintf(dealerFirstCard, "The dealer's first card is a %d.", dealerCurrentCard);
		basicTextPromptStr(infoWin, dealerFirstCard);
	}
	dealerCount += dealerCurrentCard;
	updateCardCount(cardWin, userCount, dealerCount);

	//Draw the second card for the dealer, card is "face down" (not revealed until
	dealerCurrentCard = drawCard(cardCounts);

	//Create options menu and scroll text

	char* choices[] = {
			"1) Hit",
			"2) Stand",
			(char*)NULL,
	};
	int numChoices = sizeof(choices)/sizeof(choices[0]);
	MENU_DATA* menuData = createMenuData(menuWin, choices, numChoices);

	SCROLL_TEXT* prompt = createScrollText(infoWin, "What's your next move?");

	while(keepGoing) { //While you don't stand or bust, go through game loop
		//User's Move
		textPromptWithMenu(prompt, menuData);

		if(menuData->currSelection == 1) { //Hit
			//Draw the card
			userCurrentCard = drawCard(cardCounts);
				if(userCurrentCard == 11) {
					basicTextPromptStr(infoWin, "You drew an ace.");
					userElevenAces++;
				}
				else {
					char userDrawBuf[15];
					sprintf(userDrawBuf, "You drew a %d.", userCurrentCard);
					basicTextPromptStr(infoWin, userDrawBuf);
				}
			userCount += userCurrentCard;
			updateCardCount(cardWin, userCount, dealerCount);
			//Check if user has bust, and if they can reduce an ace to prevent that from happening
			if(userCount > PLAY_TO) {
				if(userElevenAces > 0) {
					userCount -= 10;
					userElevenAces--;
					updateCardCount(cardWin, userCount, dealerCount);
				}
				else {
					char userBustBuf[31];
					sprintf(userBustBuf, "Oh no! You busted with a %d...", userCount);
					keepGoing = false;
				}
			}

			if(userCount == PLAY_TO) { //User has 21 exactly
				basicTextPromptStr(infoWin, "A perfect 21! Incredible!");
				keepGoing = false;
			}

		}
		else { //Stand
			keepGoing = false;
		}

	}

	//Reveal dealer's second card
	//Draw the second card for the user
	if(dealerCurrentCard == 11) {
		basicTextPromptStr(infoWin, "The dealer's second card is an ace.");
		userElevenAces++;
	}
	else {
		char dealerSecondCard[34]; //34 = max number of chars needed for string (for when card is a 10)
		sprintf(dealerSecondCard, "The dealer's second card is a %d.", dealerCurrentCard);
		basicTextPromptStr(infoWin, dealerSecondCard);
	}
	dealerCount += dealerCurrentCard;
	updateCardCount(cardWin, userCount, dealerCount);

	//Check if dealer got two aces in a row (dealerCount will be a 22), and reduce it to 11.
	if(dealerCount > PLAY_TO) {
		userCount -= 10;
		updateCardCount(cardWin, userCount, dealerCount);
		userElevenAces--; //Only one ace counts for eleven, not both of them
	}

	//Dealer will draw until they reach DEALER_DRAW_UP_TO or higher
	while(dealerCount < DEALER_DRAW_UP_TO) {
		dealerCurrentCard = drawCard(cardCounts);
		if(dealerCurrentCard == 11) {
			basicTextPromptStr(infoWin, "The dealer drew an ace.");
			dealerElevenAces++;
		}
		else {
			char dealerDrawBuf[22];
			sprintf(dealerDrawBuf, "The dealer drew a %d.", dealerCurrentCard);
			basicTextPromptStr(infoWin, dealerDrawBuf);
		}
		dealerCount += dealerCurrentCard;
		updateCardCount(cardWin, userCount, dealerCount);
		if(dealerCount > PLAY_TO && dealerElevenAces > 0) { //Reduce ace value to 1 if bust would occur
			dealerCount -= 10;
			dealerElevenAces--;
			updateCardCount(cardWin, userCount, dealerCount);
		}
	}

	if(dealerCount <= PLAY_TO) {
		basicTextPromptStr(infoWin, "The dealer stands.");
	}
	else {
		basicTextPromptStr(infoWin, "Oops! The dealer busted!");
	}


	/*
	 * Loosing conditions
	 * 1) User busts
	 * 2) Dealer has more than user (provided that the dealer didn't bust)
	 */
	if(userCount > PLAY_TO || (dealerCount > userCount && dealerCount <= PLAY_TO)) {
		winnings = betAmount * -1;
		char losingBuf[31]; //Amount for first part of string + any int amount
		sprintf(losingBuf, "Sorry, you've lost %d.", betAmount);
		basicTextPromptStr(infoWin, losingBuf);
	}
	/*
	 * Winning conditions
	 * 1) Dealer busts while user doesn't
	 * 2) User has higher number than dealer while neither of them bust
	 */
	else if(dealerCount > PLAY_TO || userCount > dealerCount) {
		if(naturalWin) {
			winnings = betAmount * 1.5;
		}
		else {
			winnings = betAmount;
		}
		char winningBuf[40]; //Amount for first part of string + any int amount
		sprintf(winningBuf, "Congratulations, you've won %d!", winnings);
		basicTextPromptStr(infoWin, winningBuf);
	}
	else { //Tie if neither busts and the numbers tie
		winnings = 0;
		basicTextPromptStr(infoWin, "Looks like you've tied the dealer. Nobody gets anything I suppose...");
	}

	//Delete options menu

	unpost_menu(menuData->menuptr);
	deleteMenuData(menuData);

    eraseText(titleWin);
    eraseText(cardWin);
    eraseText(infoWin);
    eraseText(menuWin);

	return winnings;
}
