/*
 * tputility.c
 *
 *  Created on: Sep 20, 2021
 *      Author: user
 */

#include "tputility.h"

int checkTimePassed(Animal* a, time_t* prevTime, char* timeFilepath) { //Passing prevTime as a pointer lets the function change it
	time_t currTime = time(NULL);
	if(currTime == (time_t)-1) {
		//fprintf(stderr, "Error: Current time cannot be obtained from system. %s\n", strerror(errno));
		return EXIT_FAILURE;
	}

	time_t difference = currTime - (*prevTime);
	unsigned int hungerSubtract = difference / SECONDS_PER_ONE_HUNGER_DECREASE * -1;
	unsigned int happinessSubtract = hungerSubtract / 2 * -1;

	updateAnimalHunger(a, hungerSubtract);
	updateAnimalHappiness(a, happinessSubtract);

	if(difference >= SECONDS_PER_ONE_HUNGER_DECREASE) { //If time passed greater than ten minutes, record new time
		FILE* timerecordFile = fopen(timeFilepath, "w+");
		*prevTime = currTime;
		fprintf(timerecordFile, "%ld", currTime);
		fclose(timerecordFile);
	}

	return EXIT_SUCCESS;
}

void writePetStatus(Animal* a, char* statusFilepath) {
	FILE* petstatusFile = fopen(statusFilepath, "w+");
	fprintf(petstatusFile, "%d\n%d\n", a->hunger, a->happiness);
	fclose(petstatusFile);
}

void writeOwnerStats(Owner* o, char* ownerFilepath) {
	FILE* ownerFile = fopen(ownerFilepath, "w+");
	fprintf(ownerFile, "%d\n%d\n%d\n%d\n", o->money, o->treats[0], o->treats[1], o->treats[2]);
	fclose(ownerFile);
}



void printPet(WIN_DATA* printWin, Animal* pet, int colorPair) {
	wattron(printWin->winptr, (COLOR_PAIR(colorPair)));
	centerInWindow(printWin, pet->body);
	wattroff(printWin->winptr, (COLOR_PAIR(colorPair)));
}


void displayPetHungerInfo(WIN_DATA* displayWin, Animal* a, Owner* o) {
	eraseText(displayWin);
	char infoStr[256]; //TODO: Specify buffer size as a macro
	sprintf(infoStr, "Hunger: %d\n\nBasic Treats: %d\nFilling Treats: %d\nGourmet Treats: %d", a->hunger, o->treats[0], o->treats[1], o->treats[2]);
	centerInWindow(displayWin, infoStr);
}

void feedPetMenu(WIN_DATA* infoWin, WIN_DATA* menuWin, Animal* a, Owner* o) {
	char* treatChoices[] = {
		"1) Feed Grub",
		"2) Feed Basic Treat",
		"3) Feed Filling Treat",
		"4) Feed Gourmet Treat",
		"5) Go Back",
		(char*)NULL
	};
	int numFeedChoices = sizeof(treatChoices)/sizeof(treatChoices[0]);

	MENU_DATA* feedingMenu = createMenuData(menuWin, treatChoices, numFeedChoices);

	post_menu(feedingMenu->menuptr);

	bool validSelection = false;
	while(!validSelection) {
		textPromptWithMenuStr(infoWin, "Feed your pet what?", feedingMenu);

		if(feedingMenu->currSelection == 1) { //Grub
			updateAnimalHunger(a, 1);
			updateAnimalHappiness(a, -2);
			basicTextPromptStr(infoWin, "That grub was okay, although a treat would be nice...");
			validSelection = true;
		}
		else if(feedingMenu->currSelection >= 2 && feedingMenu->currSelection <= 4) {
			unsigned int treatIdx = feedingMenu->currSelection - 2;
			if(o->treats[treatIdx] <= 0) {
				basicTextPromptStr(infoWin, "You don't have any of those!");
			}
			else {
				int hungerInc;
				int happinessInc;
				switch(treatIdx) {
					case 0: //Basic Treat
						hungerInc = 10;
						happinessInc = 10;
						break;
					case 1: //Filling Treat
						hungerInc = 50;
						happinessInc = 0;
						break;
					case 2: //Gourmet Treat
						hungerInc = 10;
						happinessInc = 50;
						break;
					default:
						hungerInc = 0;
						happinessInc = 0;
						break;
				}
				updateAnimalHunger(a, hungerInc);
				updateAnimalHappiness(a, happinessInc);
				o->treats[treatIdx]--;
				basicTextPromptStr(infoWin, "That was a pretty tasty treat!");
				validSelection = true;
			}
		}
		else {
			validSelection = true;
		}
	}
	unpost_menu(feedingMenu->menuptr);
	deleteMenuData(feedingMenu);
}


void shopMenu(WIN_DATA* infoWin, WIN_DATA* menuWin, Owner* o) {
	char* shopChoices[] = {
		"1) Buy Basic Treat ($10)",
		"2) Buy Filling Treat ($50)",
		"3) Buy Gourmet Treat ($100)",
		"4) Go Back",
		(char*)NULL
	};

	char* shopDescs[] = {
		"+10 Hunger, +5 Happiness",
		"+50 Hunger, +0 Happiness",
		"+25 Hunger, +50 Happiness",
		"",
		(char*)NULL
	};

	int treatCosts[] = {
			10,
			50,
			100
	};

	int numShopChoices = sizeof(shopChoices)/sizeof(shopChoices[0]);
	MENU_DATA* shopMenu = createMenuDataWithOptions(menuWin, shopChoices, shopDescs, numShopChoices);

	post_menu(shopMenu->menuptr);

	bool validSelection = false;
	while(!validSelection) {
		textPromptWithMenuStr(infoWin, "What would you like to buy?", shopMenu);

		if(shopMenu->currSelection >= 1 && shopMenu->currSelection <= 3) { //Attempt to buy a treat
			int treatIdx = shopMenu->currSelection - 1;
			if(o->money < treatCosts[treatIdx]) {
				basicTextPromptStr(infoWin, "You can't afford that.");
			}
			else if(o->treats[treatIdx] >= MAX_NUM_TREATS) {
				basicTextPromptStr(infoWin, "You can't carry any more of these.");
			}
			else {
				o->treats[treatIdx]++;
				o->money -= treatCosts[treatIdx];
				validSelection = true;
			}
		}
		else { //Go back
			validSelection = true;
		}
	}
	unpost_menu(shopMenu->menuptr);
	deleteMenuData(shopMenu);
}
