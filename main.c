/*
 * Terminal Pet Main File
 * Walker Bove
 * 6/9/2021
 */

#include <errno.h>
#include <menu.h>
#include <ncurses.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "additiongame.h"
#include "animal.h"
#include "blackjack.h"
#include "owner.h"
#include "tpgraphics.h"
#include "tputility.h"

/*
 * Number Macros
 * Contain numbers containing max sizes and time differences
 */
#define BUFSIZE 256

#define MAX_STATUS_DIGITS 5
#define MAX_TIME_DIGITS 11
#define ONE_LINE_WINDOW_SIZE 3
#define PET_PAIR 1

#define BACKSPACE_KEY 8
#define ENTER_KEY 10



/*
 * Filepath Macros
 * These specify the paths to files that store data, as well as binaries for games
 */
#define PET_STATUS_FILEPATH "./.petstatus.txt"
#define PET_INFO_FILEPATH "./.petinfo.txt"
#define OWNER_INFO_FILEPATH "./.ownerinfo.txt"
#define TIME_RECORD_FILEPATH "./.timerecord.txt"


/*
 * Main Function / Game Loop
 */
int main() {

	int EXIT_CODE = EXIT_SUCCESS;

	/*
	 * Functions to set up and configure ncurses options
	 */
	initscr();
	start_color();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	curs_set(0);


	Animal* pet = initAnimal();
	Owner* user = initOwner();
	if(pet == NULL || user == NULL) {
		endwin();
		fprintf(stderr, "Not enough memory available for Terminal Pet.\n");
		return EXIT_FAILURE;
	}


	/*
	 * Create all the windows and stored data for the basic pet display
	 */
	WIN_DATA* title;
	WIN_DATA* petDisplay;
	WIN_DATA* statsDisplay;
	WIN_DATA* blurb;
	WIN_DATA* menuBox;


	/*
	 * Display all those windows
	 */

	//Used to keep track of current row when creating display
	int currScreenRow = 0;

	//Create title window
	title = createWinData(ONE_LINE_WINDOW_SIZE, COLS, 0, 0);
	centerInWindow(title, "Terminal Pet");
	currScreenRow += title->height;

	//Create pet display window;
	petDisplay = createWinData(LINES/2, 3*COLS/4, currScreenRow, 0);
	//centerInWindow(petDisplay, testStr);

	//Create stats display window (made relative to pet display window)
	statsDisplay = createWinData(petDisplay->height, COLS - petDisplay->width, currScreenRow, petDisplay->width);
	currScreenRow += petDisplay->height;

	//Create blurb box
	blurb = createWinData(ONE_LINE_WINDOW_SIZE, COLS, currScreenRow, 0);
	currScreenRow += blurb->height;

	//Create menu display menu and create menu within
	menuBox = createWinData(LINES - currScreenRow, COLS, currScreenRow, 0);
	currScreenRow += menuBox->height;

	keypad(menuBox->winptr, true); //Keypad needed to select menu options


	/*
	 * Get the stats of the pet, as well as your fmoney count
	 */
	FILE* petstatusFile = fopen(PET_STATUS_FILEPATH, "r+"); //Attempt to open petstatusFile file. If it doesn't exist, then create it and fill it out with some default values
	if(petstatusFile == NULL) {
		petstatusFile = fopen(PET_STATUS_FILEPATH, "w+");
		fprintf(petstatusFile, "100\n100\n"); //Default values of Hunger:100, Happiness:100
	}
	else { //Read in pet stats
		char statBuffer[MAX_STATUS_DIGITS];

		fgets(statBuffer, MAX_STATUS_DIGITS, petstatusFile);
		if(ferror(petstatusFile) != 0) {
			fprintf(stderr, "Error: Pet hunger could not be read from .petstatusFile.txt. %s\n", strerror(errno));
			freeAnimal(pet);
			return EXIT_FAILURE;
		}
		else {
			pet->hunger = atoi(statBuffer);
		}

		fgets(statBuffer, MAX_STATUS_DIGITS, petstatusFile);
		if(ferror(petstatusFile) != 0) {
			fprintf(stderr, "Error: Pet happiness could not be read from .petstatusFile.txt. %s\n", strerror(errno));
			freeAnimal(pet);
			return EXIT_FAILURE;
		}
		else {
			pet->happiness = atoi(statBuffer);
		}
	}
	fclose(petstatusFile);


	/*
	 * Get the pet owner's money and treat counts
	 */
	FILE* ownerinfoFile = fopen(OWNER_INFO_FILEPATH, "r+");
	if(ownerinfoFile == NULL) {
		ownerinfoFile = fopen(OWNER_INFO_FILEPATH, "w+");
		fprintf(petstatusFile, "0\n0\n0\n0\n"); //Default values of Money: 0, all treats are 0
	}
	else { //Read in owner data
		if(readOwnerFromFile(user, ownerinfoFile) == EXIT_FAILURE) {
			freeAnimal(pet);
			freeOwner(user);
			return EXIT_FAILURE;
		}
	}
	fclose(petstatusFile);


	/*
	 * Get the data that makes up the pet's "being"
	 */

	FILE* petinfoFile = fopen(PET_INFO_FILEPATH, "r"); //Attempt to open petinfoFile file. If it doesn't exist, then go through pet creation process
	if(petinfoFile == NULL) { //Pet creation process
		basicTextPromptStr(blurb, "Welcome to Terminal Pet! Seems like you don't have a terminal pet yet. Let's get one right now.");

		/*
		 * Give the pet a species/body
		 */
		char* animalChoices[] = {
				"Dog",
				"Cat",
				"Bird",
				"Fish",
				"Alien",
				(char*)NULL,
		  };
		unsigned int numAnimalChoices = sizeof(animalChoices) / sizeof(animalChoices[0]);
		MENU_DATA* animalMenu = createMenuData(menuBox, animalChoices, numAnimalChoices);

		textPromptWithMenuStr(blurb, "First, choose which pet of these five that you want.", animalMenu);
		pet->species = animalTypes[animalMenu->currSelection - 1];
		pet->body = animalBodies[animalMenu->currSelection - 1];
		deleteMenuData(animalMenu);
		centerInWindow(petDisplay, pet->body);

		/*
		 * Give the pet a color
		 */
		char* colorChoices[] = {
				"Red",
				"Green",
				"Yellow",
				"Blue",
				"Magenta",
				"Cyan",
				"White",
				(char*)NULL,
		  };
		unsigned int numColorChoices = sizeof(colorChoices)/sizeof(colorChoices[0]);
		MENU_DATA* colorMenu = createMenuData(menuBox, colorChoices, numColorChoices);

		textPromptWithMenuStr(blurb, "Next, choose a color for your pet.", colorMenu);
		pet->color = colorMenu->currSelection;
		deleteMenuData(colorMenu);

		if(has_colors()) {
			init_pair(PET_PAIR, pet->color, COLOR_BLACK);
			printPet(petDisplay, pet, PET_PAIR);
		}
		else {
			basicTextPromptStr(blurb, "Your terminal doesn't support color, but on a colored terminal, your pet will appear in that color.");
		}

		/*
		 * Give the pet a name
		 */
		textPromptTypedTemp(blurb, "Finally, give your pet a name.", menuBox, pet->name, NAME_MAX);

		//Write everything to the petinfoFile file
		petinfoFile = fopen(PET_INFO_FILEPATH, "w+");
		fprintf(petinfoFile, "%s\n%s\n%d\n%s", pet->name, pet->species, pet->color, pet->body);

		basicTextPromptStr(blurb, "You now have a terminal pet!");

	}
	else { //Read in info for the pet
		if((readAnimalFromFile(pet, petinfoFile) == EXIT_FAILURE)) { //readAnimalFromFile returns EXIT_FAILURE if an error occurs at any point of reading in pet
			return EXIT_FAILURE;
		}
		init_pair(PET_PAIR, pet->color, COLOR_BLACK);
		printPet(petDisplay, pet, PET_PAIR);
		char petSaysHi[NAME_MAX + 9];
		sprintf(petSaysHi, "%s says hi!", pet->name);
		basicTextPromptStr(blurb, petSaysHi);
	}

	fclose(petinfoFile);

	/*
	 * Get the previously recorded time for calculations
	 */

	time_t lastRecordedTime;
	FILE* timerecordFile = fopen(TIME_RECORD_FILEPATH, "r+");
	if(timerecordFile == NULL) { //Time record doesn't exist, set previously recorded time equal to right now, and create the time record
		timerecordFile = fopen(TIME_RECORD_FILEPATH, "w+");
		lastRecordedTime = time(NULL);
		fprintf(timerecordFile, "%ld", lastRecordedTime);
	}
	else { //Create a buffer to read in the previous time,
		char timeBuffer[MAX_TIME_DIGITS];
		fgets(timeBuffer, MAX_TIME_DIGITS, timerecordFile);
		if(ferror(petstatusFile) != 0) { //Error reading in time
			fprintf(stderr, "Error: Previously recorded time could not be read from .timerecordFile.txt. %s\n", strerror(errno));
			freeAnimal(pet);
			return EXIT_FAILURE;
		}
		else { //Convert read in time to to time_t
			lastRecordedTime = atol(timeBuffer);
		}
	}
	fclose(timerecordFile);

	if(has_colors()) {
		init_pair(PET_PAIR, pet->color, COLOR_BLACK);
	}

	/*
	 * Create main menu and shop menus, as well as their respective prompts
	 */
	char* mainChoices[] = {
		"1) Feed",
		"2) Show Affection",
		"3) Play Addition Game",
		"4) Play Blackjack",
		"5) Shop",
		"6) Exit Terminal Pet",
		(char*)NULL
	};
	int numMainChoices = sizeof(mainChoices)/sizeof(mainChoices[0]);
	MENU_DATA* mainMenuData = createMenuData(menuBox, mainChoices, numMainChoices);
	SCROLL_TEXT* mainMenuPrompt = createScrollText(blurb, "What would you like to do?");

	bool playing = true;
	/*
	 * Terminal Pet "Game Loop"
	 */
	while(playing) {
		if((checkTimePassed(pet, &lastRecordedTime, TIME_RECORD_FILEPATH) == EXIT_FAILURE)) {
			EXIT_CODE = EXIT_FAILURE;
			goto CLEANUP;
		}

		char petinfoBuf[BUFSIZE];
		sprintf(petinfoBuf, "Name: %s\nHunger: %d\nHappiness: %d\nMoney: %d", pet->name, pet->hunger, pet->happiness, user->money);
		centerInWindow(statsDisplay, petinfoBuf);

		int gameScore = 0;

		textPromptWithMenu(mainMenuPrompt, mainMenuData);
		switch(mainMenuData->currSelection) {
			case 1:
				displayPetHungerInfo(statsDisplay, pet, user);
				feedPetMenu(blurb, menuBox, pet, user);
				eraseText(statsDisplay);
				break;
			case 2:
				basicTextPromptStr(blurb, "Your pet feels very loved.");
				updateAnimalHappiness(pet, 1);
				break;
			case 3:
				gameScore = playAdditionGame(title, statsDisplay, blurb, menuBox);
				user->money += gameScore;
				updateAnimalHappiness(pet, gameScore/4);
				centerInWindow(title, "Terminal Pet");
				break;
			case 4:
				gameScore = playBlackjack(user->money, title, statsDisplay, blurb, menuBox);
				user->money += gameScore;
				updateAnimalHappiness(pet, gameScore/2);
				centerInWindow(title, "Terminal Pet");
				break;
			case 5:
				shopMenu(blurb, menuBox, user);
				break;
			case 6:
				basicTextPromptStr(blurb, "Come back soon. Your pet will want to see you.");
				playing = false;
				break;
			default:
				basicTextPromptStr(blurb, "Please select a valid option.");
		}
	}

	/*
	 * Free up all data associated with Terminal Pet
	 */
	CLEANUP:
	writePetStatus(pet, PET_STATUS_FILEPATH);
	writeOwnerStats(user, OWNER_INFO_FILEPATH);

	freeAnimal(pet);

	deleteScrollText(mainMenuPrompt);
	deleteMenuData(mainMenuData);

	deleteWinData(title);
	deleteWinData(petDisplay);
	deleteWinData(statsDisplay);
	deleteWinData(blurb);
	deleteWinData(menuBox);

	endwin();

	return EXIT_CODE;
}


