#Terminal Pet Makefile
#Walker Bove
#7-13-21

CC = gcc
CFLAGS = -g -Wall -Werror -pedantic-errors

all: TerminalPet

TerminalPet: main.o additiongame.o blackjack.o tputility.o owner.o animal.o tpgraphics.o
	$(CC) $(CFLAGS) -o TerminalPet main.o additiongame.o blackjack.o tputility.o owner.o animal.o tpgraphics.o -lmenu -lncurses

main.o: main.c additiongame.h animal.h blackjack.h owner.h tpgraphics.h tputility.h
	$(CC) $(CFLAGS) -c main.c
	
additiongame.o: additiongame.c additiongame.h tpgraphics.h
	$(CC) $(CFLAGS) -c additiongame.c
	
blackjack.o: blackjack.c blackjack.h tpgraphics.h
	$(CC) $(CFLAGS) -c blackjack.c

tputility.o: tputility.c tputility.h animal.h owner.h tpgraphics.h
	$(CC) $(CFLAGS) -c tputility.c

owner.o: owner.c owner.h
	$(CC) $(CFLAGS) -c owner.c
	
animal.o: animal.c animal.h
	$(CC) $(CFLAGS) -c animal.c
	
tpgraphics.o: tpgraphics.c tpgraphics.h
	$(CC) $(CFLAGS) -c tpgraphics.c  
	

	
clean:
	rm -f *.o *~

